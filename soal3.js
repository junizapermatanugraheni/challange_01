function checkEmail(email){
    //tulis kode logic
    //PAKE REGEX
    var result;
    var regexEmail = /^([A-Za-z][A-Za-z0-9\-\.\_]*)\@([A-Za-z][A-Za-z0-9\-\_]*)(\.[A-Za-z][A-Za-z0-9\-\_]*)+$/ ;
    var regexEmailNot = /^([A-Za-z][A-Za-z0-9\-\.\_]*)\@([A-Za-z][A-Za-z0-9\-\_]*)/
    
    if(typeof email == 'string'){
        if(regexEmail.test(email)){
            result = "VALID";
        }else if(regexEmailNot.test(email)) {
            result = "INVALID";
        }else{
            result = "Tidak sesuai format";
        }
    }else if(typeof email == 'number'){
        result = "Ini bukan string"
    }else if(typeof email == 'undefined'){
        result = "Harus ada nilai, tidak boleh kosong"
    }
    return result
}


console.log(checkEmail('apranata@binar.co.id')) //OUTPUT => "VALID"
console.log(checkEmail('apranata@binar.com')) //OUTPUT => "VALID"
console.log(checkEmail('apranata@binar')) //OUTPUT => "INVALID"
console.log(checkEmail('apranata'))//OUTPUT => Bukan format email

console.log(checkEmail(3322)) //Output => bukan tipe data string
console.log(checkEmail()) //Output => Tidak ada parameter. Tidak boleh kosong

