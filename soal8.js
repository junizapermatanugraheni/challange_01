const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
  ];

  function getInfoPenjualan(dataPenjualan){
      
      //menghitung total modal
      let totalBarangTerjual = dataPenjualan.reduce((total, jual) => total + jual.totalTerjual , 0)
      let totalBarang = dataPenjualan.reduce((total, barang) => total + barang.totalTerjual + barang.sisaStok , 0) //menghitung total stok semua barang
      let totalBeli = dataPenjualan.reduce((total, beli) => total + beli.hargaBeli * totalBarang , 0) //menghitung total harga beli barang stok
      let totalJualBarang = dataPenjualan.reduce((total, jualBarang) => total + jualBarang.hargaJual * totalBarangTerjual , 0) //menghitung total harga barang terjual
      let totalModal = totalBeli//menghitung modal dari pembelian barang
      var	reverse2 = totalModal.toString().split('').reverse().join(''),
      ribuan 	= reverse2.match(/\d{1,3}/g);
      ribuanModal	= ribuan.join('.').split('').reverse().join('');
      //return totalModal

      //menghitung keuntungan
      let totalKeuntungan = totalJualBarang -  totalModal //menghitung keuntungan dari hasil penjualan
      var	reverse1 = totalKeuntungan.toString().split('').reverse().join(''),
      ribuan 	= reverse1.match(/\d{1,3}/g);
      ribuanKeuntungan	= ribuan.join('.').split('').reverse().join('');
      //return totalKeuntungan

      //menghitung persentase keuntungan
      let persentaseKeuntungan = (totalKeuntungan / totalModal) * 100
      persentaseKeuntungan = persentaseKeuntungan.toFixed(2)
      //return `${persentaseKeuntungan}%`

      
      var data = dataPenjualan.sort((a,b) => a.totalTerjual - b.totalTerjual)
      data.reverse()
      //mencari produck terlaris
      var produkBukuTerlaris = dataPenjualan[0].namaProduk
       //mencari penulis terlaris
      var penulisTerlaris = dataPenjualan[0].penulis
      //return penulisTerlaris
      const infoPenjualan = [
        {
          "Total Keuntungan: ": "Rp." + ribuanKeuntungan,
          "Total Modal:  ": "Rp." + ribuanModal,
          "Persentase Keuntungan: ": persentaseKeuntungan+"%",
          "Produk Buku Terlaris: ":produkBukuTerlaris,
          "Penulis Terlaris: ":penulisTerlaris,
        },
      ];
      return infoPenjualan
  }
  console.log(getInfoPenjualan(dataPenjualanNovel))
  