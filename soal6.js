function getAngkaTerbesarKedua(Number){
    //tulis code logic
    
    if(typeof Number == 'object'){
        Number.sort((i,j) => i-j)
        Number.reverse()
        return Number[1]
    }else if(Number == 0){
        return "Nilai null"
    }else if(typeof Number == 'undefined'){
        return "Parameter kosong"
    }
}
const dataAngka = [9,4,7,7,4,3,2,2,8]

console.log(getAngkaTerbesarKedua(dataAngka)) //output: 8

console.log(getAngkaTerbesarKedua(0)) //ERROR: ?

console.log(getAngkaTerbesarKedua()) //ERROR: ?