function checkTypeNumber(givenNumber){
    // tulis kode logic kamu didalm block ini ya
    let hasil;
    if(typeof givenNumber == 'number'){
        if(givenNumber % 2 == 0){
            hasil = "GENAP"
        }else{
            hasil = "GANJIL"
        }
    }else if(typeof givenNumber == 'string' || typeof givenNumber == 'object'){
        hasil = "ERROR: Invalid data type"
    }else{
        hasil = "ERROR: Bro where is the parameter"
    }
    return hasil;
}

console.log(checkTypeNumber(10))
console.log(checkTypeNumber(3))
console.log(checkTypeNumber("3"))
console.log(checkTypeNumber({}))
console.log(checkTypeNumber([]))
console.log(checkTypeNumber())