function getSplitName(personName){

    var data
    if(typeof personName == 'string'){
        var convert = personName.split(' ');
        if(convert.length > 1 && convert.length < 4){
            data = [{firstName: convert[0], middleName: convert.slice(1, -1).join(' '), lastName: convert[convert.length - 1]}];
        }else if(convert.length > 1 && convert.length < 3){
            data = [{firstName: convert[0], middleName: ' ', lastName: convert[convert.length - 1]}];
        }else if(convert.length < 2){
            data = [{firstName: convert[0], middleName: ' ', lastName: ' '}];
        }else if (convert.length > 3){
            data = "ERROR: This function is only for 3 characters name"
        }
    }else if(personName == 0){
        data = "ERROR: Nilai parameter 0 bukan string"
    }
    return data
}

console.log(getSplitName("Aldi Daniel Pranata"))
console.log(getSplitName("Dwi Kuncoro"))
console.log(getSplitName("Aurora"))
console.log(getSplitName("Aurora Aureliya Sukma Darma"))
console.log(getSplitName(0))